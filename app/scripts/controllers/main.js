'use strict';

/**
 * @ngdoc function
 * @name batsuitApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the batsuitApp
 */
angular.module('batsuitApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

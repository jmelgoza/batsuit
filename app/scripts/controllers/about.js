'use strict';

/**
 * @ngdoc function
 * @name batsuitApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the batsuitApp
 */
angular.module('batsuitApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
